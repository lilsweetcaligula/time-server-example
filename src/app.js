const Express = require('express')
const TimeApi = require('./time/routes.js')
const { handleAppError } = require('./middleware/handle_app_error.js')

exports.initialize = () => {
  const app = Express()

  app.use('/', TimeApi.initialize())
  app.use(handleAppError())

  return app
}
