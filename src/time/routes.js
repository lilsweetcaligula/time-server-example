const Express = require('express')
const TimeNow = require('./now/handler.js')

exports.initialize = () => 
  Express.Router()
    .get('/now', TimeNow.initialize())

