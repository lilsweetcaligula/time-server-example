const assert = require('assert-plus')
const moment = require('moment')

const Utils = {
  toIsoString: (moment_date, { show_tz = false } = {}) => {
    assert(moment_date, 'moment_date')
    assert.bool(show_tz, 'show_tz')

    return moment_date.toISOString(show_tz)
  }
}

module.exports = Utils
