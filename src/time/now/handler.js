const moment = require('moment')
const { asyncHandler } = require('../../lib/express_utils.js')
const { toIsoString } = require('./utils.js')
const { validateQuery } = require('./validation.js')

exports.initialize = () => asyncHandler(async (req, res) => {
  const { query: unsafe_query } = req
  const query_params = await validateQuery(unsafe_query)

  const now = moment.utc()

  if ('offset' in query_params) {
    with_offset = moment(now).utcOffset(query_params.offset)
  } else {
    with_offset = moment(now) 
  }

  res.json({
    unix: now.unix(),
    utc: toIsoString(now, { show_tz: false }),
    with_offset: toIsoString(with_offset, { show_tz: true })
  })

  return
})
