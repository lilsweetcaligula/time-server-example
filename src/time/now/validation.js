const { validateWith } = require('../../lib/validation.js')

const Validation = {
  validateQuery: validateWith(schema => schema.object({
    offset: schema.number().integer().optional()
  }))
}

module.exports = Validation
