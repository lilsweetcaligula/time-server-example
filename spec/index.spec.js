const Server = require('../src/server.js')
const { requireNoCache } = require('../spec/support/helpers/index.js').Import

describe('entry point', () => {
  it('initializes the server', () => {
    spyOn(Server, 'initialize')

    void requireNoCache(require, '../src/index.js')

    expect(Server.initialize).toHaveBeenCalledWith()
  })
})

