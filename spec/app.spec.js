const supertest = require('supertest')
const moment = require('moment')
const App = require('../src/app.js')
const { isoDate: matchesIsoDate } = require('./support/matchers.js')(jasmine)

describe('app', () => {
  describe('requesting the time now', () => {
    const now = '2020-01-13T12:30:00Z'

    beforeAll(() => {
      jasmine.clock().mockDate(new Date(now))
    })

    const requestTimeNow = () => supertest(App.initialize())
      .get('/now')

    describe('on bad query', () => {
      it('responds with 400 Bad Data and an error message', async () => {
        const bad_query = { unknown: 'prop' }

        await requestTimeNow()
          .query(bad_query)
          .expect(400)
          .expect(res => {
            expect(res.body).toEqual({
              error: { message: '"unknown" is not allowed' }
            })
          })
      })
    })

    describe('on success', () => {
      it('responds with the info about time now', async () => {
        await requestTimeNow()
          .expect(200)
          .expect(res => {
            expect(res.body).toEqual({
              unix: 1578918600,
              utc: '2020-01-13T12:30:00.000Z',
              with_offset: jasmine.any(String) 
            })
          })
      })

      describe('offset param', () => {
        it('applies the offset if the "offset" param is specified', async () => {
          await requestTimeNow()
            .query({ offset: -7 })
            .expect(200)
            .expect(res => {
              expect(res.body).toEqual({
                unix: 1578918600,
                utc: '2020-01-13T12:30:00.000Z',
                with_offset: '2020-01-13T05:30:00.000-07:00'
              })
            })
        })

        it('has the offset equal to the UTC time when no offset is applied', async () => {
          await requestTimeNow()
            .expect(200)
            .expect(res => {
              expect(res.body).toEqual(jasmine.objectContaining({
                utc: '2020-01-13T12:30:00.000Z',
                with_offset: '2020-01-13T12:30:00.000+00:00',
              }))
            })
        })
      })
    })
  })
})

