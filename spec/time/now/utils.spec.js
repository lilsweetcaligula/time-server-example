const { toIsoString } = require('../../../src/time/now/utils.js')
const moment = require('moment')

describe('GET /time/now utils', () => {
  describe('toIsoString', () => {
    const date = moment()
    const fake_iso_string = { _fake: 'iso string' }

    beforeEach(() => {
      spyOn(date, 'toISOString').and.returnValue(fake_iso_string)
    })

    it('delegates to the "moment" package', () => {
      const result = toIsoString(date, { show_tz: true })

      expect(result).toEqual(fake_iso_string)
      expect(date.toISOString).toHaveBeenCalledWith(true)
    })

    it('the "show_tz" option defaults to false if not provided', () => {
      const result = toIsoString(date)

      expect(result).toEqual(fake_iso_string)
      expect(date.toISOString).toHaveBeenCalledWith(false)
    })
  })
})
